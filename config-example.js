
//Use this to store settings for the environment

//		**** YOU NEED TO RENAME THIS FILE config.js  ******

var config = {};

config.mandrill = {};

config.mandrill.api_key = 'YOUR_API_KEY';
config.mandrill.template = 'YOUR_TEMPLATE_SLUG';

module.exports = config;