//This file shows how to call the send-template.js file from another place in the project

//This is a simple send script that only sets a few vars, the rest are defaults in the send-template.js file


var email = "fred@mailmob.co.uk";
var name = "Fred";
var base_url = "http://www.google.co.uk/search?q=";
var unique_ref = "A1457"

//var product_type = "Loan";
//var promo = "Don't forget, the special introductory ofer of 6.5% APR for loans up to £6,000 will only be available until January 31st.";


var product_type = "Credit Card";
var promo = "Don't forget, the special introductory ofer of 0% on balance transfers for 6 months will only be available until January 31st.";


//var product_type = "Savings";
//var promo = "Don't forget, the special introductory ofer of 4.0% APR for deposits up to £10,000 will only be available until January 31st.";




var sender = require('../send-template.js');

sender.template_sender(name,email,product_type,promo,base_url,unique_ref);