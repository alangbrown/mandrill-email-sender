module.exports.template_sender = template_sender;

var config = require('./config');  //Load the config file to get API KEY etc

var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(config.mandrill.api_key);

function template_sender(name,email,product_type,promo,base_url,unique_ref){



var template_name = config.mandrill.template;

var template_content = [{

    }];

var message = {
    "subject": "Thanks for your " + product_type + " application",
    "from_email": "mandrill@mailmob.co.uk",
    "from_name": "Interact Bank",
    "to": [{
            "email": email,
            "name": name,
            "type": "to"
        }],
    "headers": {
        "Reply-To": "mandrill@mailmob.co.uk"
    },
    "important": false,
    "track_opens": null,
    "track_clicks": null,
    "auto_text": null,
    "auto_html": null,
    "inline_css": true,
    "url_strip_qs": null,
    "preserve_recipients": null,
    "view_content_link": null,
    "tracking_domain": null,
    "signing_domain": null,
    "return_path_domain": null,
    "merge": true,
    "global_merge_vars": [
        {
            "name": "product_type",
            "content": product_type
        },
        {
            "name": "fname",
            "content": name
        },
        {
            "name": "unique_ref",
            "content": unique_ref
        },
        {
            "name": "promo",
            "content": promo
        },
        {
            "name": "base_url",
            "content": base_url
        }

    ]
};
var ip_pool = "Main Pool";
mandrill_client.messages.sendTemplate({"template_name": template_name, "template_content": template_content, "message": message, "ip_pool": ip_pool}, function(result) {
    console.log(result);
    /*
    [{
            "email": "recipient.email@example.com",
            "status": "sent",
            "reject_reason": "hard-bounce",
            "_id": "abc123abc123abc123abc123abc123"
        }]
    */
}, function(e) {
    // Mandrill returns the error as an object with name and message keys
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
});

}